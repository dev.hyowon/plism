const express = require('express');
const njk = require('nunjucks');

const app = express();

njk.configure('src/', {
  autoescape: true,
  express: app,
  watch: true,
});

app.use(express.static('static'));

app.get('/', function (req, res) {
  res.render('pages/index.njk');
});

app.get('/*.html$/', function (req, res) {
  res.render(`pages/${req.params[0]}.njk`);
});

app.listen(3000);
