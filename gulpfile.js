const { src, dest, watch, series, parallel } = require('gulp');
const njk = require('gulp-nunjucks-render');
const prettier = require('gulp-prettier');
const del = require('del');
const pkg = require('./package');
const postcss = require('gulp-postcss');
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const nodemon = require('nodemon');
const browserSync = require('browser-sync');

sass.compiler = require('node-sass');

function devCSS() {
  const plugins = [autoprefixer()];
  return src('assets/sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(plugins))
    .pipe(dest('dist/assets/css'));
}

function prodCSS() {
  const plugins = [autoprefixer()];
  return src('assets/sass/*.scss')
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(postcss(plugins))
    .pipe(dest('build/assets/css'));
}

function clean(path) {
  return function clean() {
    return del(path);
  };
}

function html() {
  const options = {
    path: ['src/'],
    manageEnv: function (env) {
      env.addGlobal('STATIC_URL', pkg.config.staticUrl || '.');
      env.addGlobal('TITLE', pkg.config.title || '.');
    },
  };
  return src('src/pages/**/*.njk')
    .pipe(njk(options))
    .pipe(prettier())
    .pipe(dest('build'));
}

function watches() {
  watch('assets/sass/**/*.scss', { delay: 500 }, series(devCSS, browserReload));
  watch('assets/js/*.js', { delay: 500 }, browserReload);
  watch('src/**/*.njk', { delay: 500 }, browserReload);
}

function browserReload(cb) {
  browserSync.reload();
  cb();
}

function server() {
  nodemon({
    script: './app',
  }).once('start', () => console.log(';;;zzz'));
}

function app() {
  browserSync.init({
    proxy: 'localhost:3000',
    serveStatic: [
      { route: '/assets/css', dir: './dist/assets/css' },
      { route: '/assets/js', dir: './assets/js' },
      { route: '/assets/img', dir: './assets/img' },
      { route: '/assets/fonts', dir: './assets/fonts' },
      { route: '/assets/vendors', dir: './assets/vendors' },
    ],
    port: 3001,
    open: false,
  });
}

function copy() {
  return src('assets/**').pipe(dest('build/assets'));
}

exports.default = series(devCSS, parallel(server, app, watches));
exports.build = series(clean('build'), copy, prodCSS, html);
